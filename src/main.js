import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import './filters'
import SideBar from './layouts/SideBar'
import Default from './layouts/Default'
import Vuetify from 'vuetify'
import './assets/css/theme.css'


Vue.use(Vuetify)
Vue.config.productionTip = false

Vue.component('side-bar-layout', SideBar)
Vue.component('default-layout', Default)

new Vue({
  router,
  store,
  render: function (h) { return h(App) }
}).$mount('#app')
