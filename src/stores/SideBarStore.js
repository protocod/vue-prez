export default {
    state: {
      sidebar: {
        color: 'vue-secondary'
      }
    },
    mutations: {
      CHANGE_SIDEBAR_COLOR: (state, color) => {
        state.sidebar.color = color
      }
    },
    getters: {
      sidebarColor: state => {
        return state.sidebar.color
      }
    }
  }