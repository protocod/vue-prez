export default {
    state: {
      todos: [],
      idTodo: 0,
      input: '',
      selectAll: false
    },
    mutations: {
      ADD_TODO: (state, todo) => {
        const id = state.idTodo
        state.todos = [...state.todos, { id, content: todo, checked: false}]
        state.idTodo = id+1
      },
      REMOVE_TODO: (state, checkedTodo) => {
        const todolist = state.todos.filter(todo => !checkedTodo.some(checked => todo.id === checked.id))
        state.todos = todolist
      },
      RESET_TODOLIST: state => {
        const todolist = state.todos.map(todo => {
          todo.checked = false
          return todo
        })
        state.selectAll = false
        state = todolist
      },
      HANDLE_INPUT: (state, value) => {
        state.input = value
      },
      HANDLE_CHECK: (state, checked) => {
        state.todos = state.todos.map(todo => {
          if (todo.id === checked.id)
            todo.checked = !todo.checked
          return todo
        })
      },
      RESET_SELECT_ALL: state => {
        state.selectAll = false
      },
      HANDLE_SELECT_ALL: state => {
        state.selectAll = !state.selectAll
        state.todos = state.todos.map(todo => {
          todo.checked = state.selectAll
          return todo
        })
      }
    },
    getters: {
      todolist: state => {
        return state.todos
      },
      hasTodo: state => {
        return state.todos.length > 0
      },
      isChecked: state => {
        return state.todos.filter(todo => todo.checked).length > 0
      },
      checkedTodo: state => {
        return state.todos.filter(todo => todo.checked)
      },
      currentInput: state => {
        return state.input
      },
      getSelectAll: state => {
        return state.selectAll
      }
    }
  }