import Vue from 'vue'

const upper = (text) => text.toUpperCase()
const lower = (text) => text.toLowerCase()
const truncate = (text, length) => `${text.substring(0, length)}...`
const ucfirst = (text) => `${text.charAt(0).toUpperCase()}${text.slice(1)}`
const ucwords = (words) => words.split(' ').map(word => ucfirst(word)).join(' ')
const lcfirst = (text) => `${text.charAt(0).toLowerCase()}${text.slice(1)}`
const lcwords = (words) => words.split(' ').map(word => lcfirst(word)).join(' ')

Vue.filter('upper', upper)
Vue.filter('lower', lower)
Vue.filter('truncate', truncate)
Vue.filter('ucfirst', ucfirst)
Vue.filter('ucwords', ucwords)
Vue.filter('lcfirst', lcfirst)
Vue.filter('lcwords', lcwords)