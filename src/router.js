import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'welcome',
      component: () => import('./views/Welcome.vue')
    },
    {
      path: '/home',
      name: 'introduction',
      meta: { sidebar: true },
      component: () => import('./views/Home.vue')
    },
    {
      path: '/about',
      name: 'about',
      meta: { sidebar: true },
      component: () => import('./views/About.vue')
    },
    {
      path: '/todolist',
      name: 'todolist',
      meta: { sidebar: true },
      component: () => import('./views/Todos.vue')
    },
    {
      path: '*',
      name: 'not-found',
      component: () => import('./views/PageNotFound.vue')
    },
  ]
})
