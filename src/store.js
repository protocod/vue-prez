import Vue from 'vue'
import Vuex from 'vuex'
import SideBarStore from '@/stores/SideBarStore'
import TodoListStore from '@/stores/TodoListStore'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    todolist: TodoListStore,
    sidebar: SideBarStore
  }
})
